require 'csv'

namespace :csv do
  desc "Importa o arquivo dados.csv"
  task import: :environment do
    CSV.foreach('lib/boardgames.CSV', col_sep: ';', :encoding => 'windows-1252:utf-8').with_index do |linha, indice|
      unless (indice == 0)
        Boardgame.create!(name: linha[0], copy: linha[1], category_id: linha[2], situation_id: linha[3], remote_image_url: linha[4])
      end
    end
  end
end
