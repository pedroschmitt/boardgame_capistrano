source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '~> 5.2.1'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'bootsnap', '>= 1.1.0', require: false

gem 'rails_admin'
gem 'rails_admin_rollincode', '~> 1.0'
gem 'cancancan'
gem 'devise'
# Gem para gerar os pdfs
gem 'prawn-rails'
# Gem for scan uploads
gem 'carrierwave'
gem 'bulma-rails', '~> 0.7.1'
gem 'simple_form', '~> 4.0', '>= 4.0.1'
# Gem for cloud upload, with carrierwave
gem 'cloudinary'
gem 'cpf_utils', '~> 1.3.1'
gem 'paper_trail'
gem 'ed25519'
gem 'bcrypt_pbkdf'

group :development, :test do
  # gem 'sqlite3', '1.3.13'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rails_real_favicon'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # Create a pdf with the models (database)
  gem 'rails-erd'
  # deploy with capristano
  gem "capistrano", "~> 3.10", require: false
  gem "capistrano-rails", "~> 1.3", require: false
  gem 'capistrano3-puma', require: false
  gem 'capistrano-rvm', require: false
end

group :production do
  gem 'pg', '0.20.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
