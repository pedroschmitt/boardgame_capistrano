class Boardgame < ApplicationRecord
  # enum copy: [:copy_1, :copy_2, :copy_3, :copy_4, :copy_5]
  enum copy: ['cópia 1', 'cópia 2', 'cópia 3', 'cópia 4', 'cópia 5']
  belongs_to :category
  belongs_to :situation
  has_many :rents
  has_paper_trail
  
  mount_uploader :image, ImageUploader
end
