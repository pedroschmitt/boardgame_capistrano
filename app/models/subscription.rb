class Subscription < ApplicationRecord
  enum status: [:active, :inactive, :suspended, :overdue]
  belongs_to :client
  belongs_to :plan
  belongs_to :user
  has_paper_trail
  
  validates :subscription_date, presence: true
  
end
