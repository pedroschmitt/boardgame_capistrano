class Rent < ApplicationRecord
  enum status: [:rented, :returned, :overdue]
  belongs_to :client
  belongs_to :boardgame
  belongs_to :plan
  belongs_to :user
  has_paper_trail
  
  delegate :client, to: :boardgame, prefix: true
  validates :rent_date, presence: true

  
  # impede a criacao de um aluguel de jogos fixos/nao locaveis
  before_create do
    if !self.boardgame.category.rentable
      throw :abort
    end
    # not allow to rent a already rented boardgame
    if status == "rented"
      if self.boardgame.situation_id != 1
        throw :abort
      end
    end
  end
  
  
  # Put automatically the expected return date for each category
  before_create do
      write_attribute(:expected_return_date, rent_date + (self.boardgame.category.days).days)
  end
  
  after_save do
    if status == "rented"
      self.boardgame.update( situation_id: 2)
    elsif status == "returned"
      self.boardgame.update( situation_id: 1)
    elsif status == "overdue"
      self.boardgame.update( situation_id: 2)  
    end
  end
  
   # Virtual field method to show on boardgame admin view
  def rental_client
    if status == "rented"
      "#{self.client.name}, no prazo."
    elsif status == "overdue"
      "#{self.client.name}, atrasado!"
    end
  end
  
  # Virtual field method to show on client admin view
  def rented_games_client
    "#{self.boardgame.name}"
  end

  # Virtual field method
  def rental_boardgame
    [self.client, self.boardgame].compact.join(', ')
  end
  
  # run  $ rails runner "Rent.check_overdue"
  def self.check_overdue
    rent = Rent.where(status: 'rented').where("expected_return_date < ?", Date.today).all
    #1.day.ago does not work, because rails put time, and find no records, since this is only date, not datetime)
    rent.update(status: 'overdue')
  end

end
