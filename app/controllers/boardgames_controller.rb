class BoardgamesController < ApplicationController
  def index
    # @boardgames = Boardgame.all.order("name ASC")
    
    # search form https://medium.com/@zylberberg.jonathan/creating-a-search-form-in-rails-5-77fdef6be74d
    if params[:boardgame]
      @boardgames = Boardgame.where('LOWER(name) ILIKE ?', "%#{params[:boardgame]}%")
      #for case insensitive, in Postgress and SQLite (ILIKE broke in SQLite)
      #https://stackoverflow.com/questions/6892034/postgres-case-insensitive-searching-with-rails
      # @boardgames = Boardgame.where("LOWER(name) LIKE LOWER('%#{params[:boardgame]}%')")
                                    
    else
      @boardgames = Boardgame.all.order("name ASC")
    end
    
  end
  
  def show
  end
  
  def boardgame_params
    params.require(:boardgame).permit(:name)
  end
end
