RailsAdmin.config do |config|
  
  require Rails.root.join('lib', 'rails_admin', 'rails_admin_pdf.rb')
  RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::Pdf)
  
  config.main_app_name = ["Ludoteca BGC", " - acervo de jogos"]
  
  config.navigation_static_label = "Links Úteis"
  config.navigation_static_links = {
    'Planilha antiga' => 'https://docs.google.com/spreadsheets/d/1xwpTE4qmS3u5k21kcztwc2SW_aABfOurtei39d2EjT4/edit#gid=958630188'
  }
  
  
  ### Popular gems integration

  # == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  # == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  ## Aumentar o tamanho da largura, para caberem mais informacoes
  # config.total_columns_width = 1500
  
  # Configure the default number of rows rendered per page
  # https://github.com/sferik/rails_admin/wiki/List
  config.default_items_per_page = 20

  config.model Boardgame do
    object_label_method do
      :bg_name_copy
    end
    
    configure :image_link do
      hide
    end
    
    create do
      configure  :copy do
        default_value do
          default_value 0
        end
      end
      configure  :situation do
        default_value do
          default_value 1
        end
      end
      configure :rents do
        hide
      end
    end
    
    list do
      field  :name
      field  :copy
      field  :situation
      field  :category
      field  :rents do
        pretty_value do
          bindings[:object].rents.all.map {|r| r.rental_client}.join
        end
      end
      field  :notes
    end
  end
  
    
  # Change the name showed in another relations (rent, situations, etc)
  # Boardgame.class_eval do
  def bg_name_copy
   "#{self.name} - #{self.copy}"
  end
  # end
    
    
  config.model Client do
    navigation_icon 'fa fa-id-card'
    configure :birthday, :date do
      date_format :default # Change name for numbers, so user can digit.
    end
    
    create do
      configure :subscription do
        hide
      end
      configure :rents do
        hide
      end
      configure  :status do
        default_value do
          default_value 0
        end
      end
    end

    edit do
      configure :rents do
        hide
      end
    end
    
    show do
       # Virtual field that show the boardgames the client rented
      configure  :rented_games_client do
        formatted_value{ bindings[:object].rents.where(status: ['rented', 'overdue']).map {|r| r.rented_games_client}.join(', ') }
      end
       # Virtual field that show the overdue boardgames the client rented
      configure  :overdue_games_client do
        formatted_value{ bindings[:object].rents.where(status: 'overdue').map {|r| r.rented_games_client}.join(', ') }
      end
    end
    
    list do
      field  :name
      # Virtual field that show how many rents the client has
      field  :rented_games_count do
        pretty_value do
          bindings[:object].rents.where(status: ['rented', 'overdue']).count
        end
      end
      # Virtual field that show how many overdue rents the client has
      field  :overdue_games_count do
        pretty_value do
          bindings[:object].rents.where(status: 'overdue').count
        end
      end
      # Virtual field that show the boardgames the client rented
      field  :rented_games_client do
        formatted_value{ bindings[:object].rents.where(status: ['rented', 'overdue']).map {|r| r.rented_games_client}.join(', ') }
      end
      # Virtual field that show the overdue boardgames the client rented
      field  :overdue_games_client do
        formatted_value{ bindings[:object].rents.where(status: 'overdue').map {|r| r.rented_games_client}.join(', ') }
      end
    end
    
    show do
      
    end
  end
  
  
  config.model Rent do
    navigation_icon 'fa fa-refresh'
    create do
      field :client
      field :plan
      field :boardgame
      field :rent_date do
        default_value I18n.l(DateTime.now, format: :long)
      end
      # field :expected_return_date do
      #   default_value I18n.l(DateTime.now + 7.days, format: :long)
      # end
      field :status do
        default_value do
          default_value 0
        end
      end
      field :notes
      field :user_id, :hidden do
        default_value do
          bindings[:view]._current_user.id
        end
      end
    end
    edit do
      field :status do
        default_value do
          default_value 1
        end
      end
      field :returned_date
      field :boardgame
      field :client
      field :plan
      field :rent_date
      field :expected_return_date
      field :status do
        default_value do
          default_value 1
        end
      end
      field :notes
      field :user_id, :hidden do
        default_value do
          bindings[:view]._current_user.id
        end
      end
    end
    
    list do
      scopes [:rented, :returned, :overdue]
      field :boardgame do
        searchable :name
      end
      field :client do
        searchable :name
      end
      field :status
      field :rent_date
      field :returned_date
      field :expected_return_date
    end
      
  end
  
  config.model Subscription do
    create do
      field :client
      field :plan
      field :subscription_date do
        default_value I18n.l(DateTime.now)
      end
      field :day_of_charge
      field :notes
      field :status do
        default_value do
          default_value 0
        end
      end
      field :user_id, :hidden do
        default_value do
          bindings[:view]._current_user.id
        end
      end
    end
    edit do
      field :status
      field :notes
      field :client
      field :plan
      field :subscription_date
      field :day_of_charge
    end
  end

  config.model Plan do
    navigation_icon 'fa fa-tags'
    parent Client
    list do
      field :name
      field :value
      field :boardgame_quantity
    end
  end
  
  config.model Boardgame do
    navigation_icon 'fa fa-cubes'
    weight -1
  end
  
  config.model Category do
    navigation_icon 'fa fa-adjust'
   list do
      field  :name
      field  :value
      field  :days
      field  :rentable
    end
  end
  
  config.model Subscription do
    navigation_icon 'fa fa-cc-visa'
    parent Client
  end
  
  config.model Situation do
    navigation_icon 'fa fa-sort-amount-asc'
  end
  
  config.model User do
    navigation_icon 'fa fa-user'
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    pdf do
      only Client
    end
  
    PAPER_TRAIL_AUDIT_MODEL = ['Order', 'Payment']
    config.actions do
      history_index do
        only PAPER_TRAIL_AUDIT_MODEL
      end
      history_show do
        only PAPER_TRAIL_AUDIT_MODEL
      end
    end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
