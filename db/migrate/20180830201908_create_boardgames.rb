class CreateBoardgames < ActiveRecord::Migration[5.2]
  def change
    create_table :boardgames do |t|
      t.string :name
      t.integer :copy
      t.references :category, foreign_key: true
      t.references :situation, foreign_key: true
      t.string :image_link
      t.text :notes

      t.timestamps
    end
  end
end
