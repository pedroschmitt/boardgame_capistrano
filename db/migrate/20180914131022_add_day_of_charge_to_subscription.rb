class AddDayOfChargeToSubscription < ActiveRecord::Migration[5.2]
  def change
    add_column :subscriptions, :day_of_charge, :integer
  end
end
