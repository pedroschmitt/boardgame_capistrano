class AddBoardgameQuantityToPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :plans, :boardgame_quantity, :integer
  end
end
