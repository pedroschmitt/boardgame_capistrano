class AddImageToBoardgames < ActiveRecord::Migration[5.2]
  def change
    add_column :boardgames, :image, :string
  end
end
