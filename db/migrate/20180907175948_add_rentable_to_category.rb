class AddRentableToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :rentable, :boolean
  end
end
