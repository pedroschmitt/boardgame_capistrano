# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_07_223113) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boardgames", force: :cascade do |t|
    t.string "name"
    t.integer "copy"
    t.bigint "category_id"
    t.bigint "situation_id"
    t.string "image_link"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.index ["category_id"], name: "index_boardgames_on_category_id"
    t.index ["situation_id"], name: "index_boardgames_on_situation_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "days"
    t.boolean "rentable"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "document"
    t.string "email"
    t.string "phone"
    t.text "address"
    t.text "notes"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "birthday"
    t.string "documentphoto"
  end

  create_table "plans", force: :cascade do |t|
    t.string "name"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "boardgame_quantity"
  end

  create_table "rents", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "boardgame_id"
    t.bigint "plan_id"
    t.date "rent_date"
    t.date "expected_return_date"
    t.bigint "user_id"
    t.text "notes"
    t.date "returned_date"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["boardgame_id"], name: "index_rents_on_boardgame_id"
    t.index ["client_id"], name: "index_rents_on_client_id"
    t.index ["plan_id"], name: "index_rents_on_plan_id"
    t.index ["user_id"], name: "index_rents_on_user_id"
  end

  create_table "situations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "plan_id"
    t.date "subscription_date"
    t.date "subscription_cancelation_date"
    t.integer "status"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.integer "day_of_charge"
    t.index ["client_id"], name: "index_subscriptions_on_client_id"
    t.index ["plan_id"], name: "index_subscriptions_on_plan_id"
    t.index ["user_id"], name: "index_subscriptions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.integer "kind"
    t.integer "status"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "boardgames", "categories"
  add_foreign_key "boardgames", "situations"
  add_foreign_key "rents", "boardgames"
  add_foreign_key "rents", "clients"
  add_foreign_key "rents", "plans"
  add_foreign_key "rents", "users"
  add_foreign_key "subscriptions", "clients"
  add_foreign_key "subscriptions", "plans"
  add_foreign_key "subscriptions", "users"
end
